#include "Graph.h"

Graph::Graph(){
	this->startNode = nullptr;
	this->endNode = nullptr;
	this->maxEdgeLength = 0;
}

void Graph::initalGraph(){
	for (int i = 0; i < nodeList.size(); i++){
		nodeList.at(i)->setUnmarked();
		nodeList.at(i)->setCurrentDistance(std::numeric_limits<int>::max());
	}
	edgeOrderList.clear();
}

void Graph::putNode(Node *node){
	nodeList.push_back(node);
}

Node *Graph::getNode(int nodeIndex){
	return nodeList.at(nodeIndex);
}

void Graph::putEdgeToNode(int nodeIndex, Edge *edge){
	nodeList.at(nodeIndex)->putEdge(edge);
}

Edge *Graph::getNodeEdge(int nodeIndex, int edgeIndex){
	return nodeList.at(nodeIndex)->getEdge(edgeIndex);
}

int Graph::graphSize(){
	return nodeList.size();
}

int Graph::nodeEdgeListSize(int nodeIndex){
	return nodeList.at(nodeIndex)->edgeListSize();
}

void Graph::breadthFirstSearch(){
	initalGraph();
	queue<Node *> nodeQueue;
	nodeQueue.push(nodeList.at(0));
	nodeList.at(0)->setMarked();
	Node *tempNode = nullptr;
	while (!nodeQueue.empty()){
		tempNode = nodeQueue.front();
		if (!tempNode->isEdgeListEmpty()){
			int i = 0;
			while (i != tempNode->edgeListSize()){
				if (!tempNode->isNextNodeMarked(i)){
					nodeQueue.push(tempNode->getNextNode(i));
					tempNode->getNextNode(i)->setMarked();
					edgeOrderList.push_back(tempNode->getEdge(i));
				}
				i++;
			}
		}
		nodeQueue.pop();
	}
}

void Graph::depthFirstSearch(){
	initalGraph();
	stack<Node *> nodeStack;
	nodeStack.push(nodeList.at(0));
	nodeList.at(0)->setMarked();
	Node *tempNode = nullptr;
	while (!nodeStack.empty()){
		tempNode = nodeStack.top();
		if (!tempNode->isEdgeListEmpty() && !tempNode->isAllNextNodeMarked()){
			int i = 0;
			while (tempNode->isNextNodeMarked(i)){
				i++;
			}
			nodeStack.push(tempNode->getNextNode(i));
			tempNode->getNextNode(i)->setMarked();
			edgeOrderList.push_back(tempNode->getEdge(i));
		}
		else{
			nodeStack.pop();
		}
	}
}

void Graph::DijkstraShortestPath(){
	initalGraph();
	CircleList *circleList = new CircleList(maxEdgeLength+1);
	circleList->setInititalNode(nodeList.at(0));
	while (nodeList.size() != circleList->getCurrentMarkedNumber())
		circleList->trivalNode();
	delete circleList;
}

string Graph::printEdgeOrder(){
	ostringstream outputBuffer;
	for (int i = 0; i < edgeOrderList.size(); i++){
		outputBuffer << "(";
		outputBuffer << edgeOrderList.at(i)->getPrevNodeIndex();
		outputBuffer << ", ";
		outputBuffer << edgeOrderList.at(i)->getNextNodeIndex();
		outputBuffer << ")" << endl;
	}
	cout << outputBuffer.str();
	return outputBuffer.str();
}

void Graph::setMaxEdgeLength(int maxEdgeLength){
	this->maxEdgeLength = maxEdgeLength;
}

int Graph::getMaxEdgeLength(){
	return maxEdgeLength;
}

void Graph::printNodeK(){
	/*for (int i = 0; i < nodeList.size(); i++)
		cout << "Node" << i << ": " << nodeList.at(i)->getCurrentIndex() << endl;*/
	cout << "List k of nodes:" << endl;
	cout << "Node a: " << nodeList.at(0)->getCurrentIndex() << endl;
	cout << "Node b: " << nodeList.at(1)->getCurrentIndex() << endl;
	cout << "Node c: " << nodeList.at(2)->getCurrentIndex() << endl;
	cout << "Node d: " << nodeList.at(3)->getCurrentIndex() << endl;
	cout << "Node e: " << nodeList.at(4)->getCurrentIndex() << endl;
	cout << "Node z: " << nodeList.at(5)->getCurrentIndex() << endl;
}

Graph::~Graph(){
	for (int i = 0; i < nodeList.size(); i++)
		delete nodeList.at(i);
}