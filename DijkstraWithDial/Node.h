#ifndef __Node_H__
#define __Node_H__
#include <vector>
#include "Edge.h"
using namespace std;
class Edge;
class Node{
private:
	vector<Edge *> edgeList;
	bool marked;
	int currentIndex;
	int currentDistance;
public:
	Node();
	void putEdge(Edge *edge);
	Edge *getEdge(int edgeIndex);
	int edgeListSize();
	void setMarked();
	void setUnmarked();
	bool isEdgeListEmpty();
	bool isMarked();
	bool isNextNodeMarked(int edgeIndex);
	bool isAllNextNodeMarked();
	Node *getNextNode(int edgeIndex);
	void setCurrentIndex(int currentIndex);
	int getCurrentIndex();
	void setCurrentDistance(int currentDistance);
	int getCurrentDistance();
	~Node();
};
#endif