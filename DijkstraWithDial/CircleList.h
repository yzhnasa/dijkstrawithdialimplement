#ifndef __CircleList_H
#define __CircleList_H
#include <vector>
#include "Node.h"
#include "Graph.h"

class CircleList{
private:
	int listLength;
	int currentIndex;
	int markedNumber;
	vector<vector<Node *>*> circleList;
	int calculateK(Node *node);
	void removeNode(Node *node, int index);
	void movingNode(Node *node);
	bool isNodeEmpty(int index);
public:
	CircleList();
	CircleList(int listLength);
	void setInititalNode(Node *node);
	void trivalNode();
	int getCurrentMarkedNumber();
	~CircleList();
};
#endif