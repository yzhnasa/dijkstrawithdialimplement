#include "CircleList.h"

CircleList::CircleList(){

}

CircleList::CircleList(int listLength){
	this->listLength = listLength;
	vector<Node *> *tempNodeList = nullptr;
	for (int i = 0; i < listLength; i++){
		tempNodeList = new vector<Node *>();
		circleList.push_back(tempNodeList);
	}
	currentIndex = 0;
	markedNumber = 0;
}

int CircleList::calculateK(Node *node){
	return node->getCurrentDistance() % listLength;
}

void CircleList::removeNode(Node *node, int index){
	int i = 0;
	Node *tempNode = nullptr;
	while (tempNode != node && i < circleList.at(index)->size()){
		tempNode = circleList.at(index)->at(i);
		i++;
	}
	if (tempNode == node)
		circleList.at(index)->erase(circleList.at(index)->begin() + i - 1);
}

void CircleList::movingNode(Node *node){
	removeNode(node, node->getCurrentIndex());
	int newK = calculateK(node);
	node->setCurrentIndex(newK);
	circleList.at(newK)->push_back(node);
}

bool CircleList::isNodeEmpty(int index){
	if (circleList.at(index)->size() == 0)
		return true;
	return false;
}

void CircleList::setInititalNode(Node *node){
	node->setMarked();
	node->setCurrentDistance(0);
	movingNode(node);
	markedNumber++;
	Node *tempNode = nullptr;
	Edge *tempEdge = nullptr;
	for (int i = 0; i < node->edgeListSize(); i++){
		tempEdge = node->getEdge(i);
		tempNode = tempEdge->getNextNode();
		tempNode->setCurrentDistance(tempEdge->getDistance());
		movingNode(tempNode);
	}
}

void CircleList::trivalNode(){
	if (currentIndex == listLength - 1)
		currentIndex = 0;
	Node *tempNode = nullptr;
	Node *tempNextNode = nullptr;
	Edge *tempEdge = nullptr;
	if (!isNodeEmpty(currentIndex)){
		for (int i = 0; i < circleList.at(currentIndex)->size(); i++){
			tempNode = circleList.at(currentIndex)->at(i);
			if (!tempNode->isMarked()){
				for (int j = 0; j < tempNode->edgeListSize(); j++){
					tempEdge = tempNode->getEdge(j);
					tempNextNode = tempEdge->getNextNode();
					if (tempNode->getCurrentDistance() + tempEdge->getDistance() < tempNextNode->getCurrentDistance()){
						tempNextNode->setCurrentDistance(tempNode->getCurrentDistance() + tempEdge->getDistance());
						movingNode(tempNode->getNextNode(j));
					}
				}
				tempNode->setMarked();
				markedNumber++;
			}
		}
	}
	currentIndex++;
}

int CircleList::getCurrentMarkedNumber(){
	return markedNumber;
}

CircleList::~CircleList(){
	for (int i = 0; i < circleList.size(); i++)
		delete circleList.at(i);
}