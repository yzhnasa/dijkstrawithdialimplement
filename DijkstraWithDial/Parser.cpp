#include "Parser.h"

void Parser::splitString(string &lineBuffer, vector<string> &tokens){
	int i = 0;
	vector<char> token;
	while (i != lineBuffer.size()){
		if (lineBuffer.at(i) != ' '){
			token.push_back(lineBuffer.at(i));
		}
		else{
			if (!token.empty())
				tokens.push_back(string(token.begin(), token.end()));
			token.clear();
		}
		i++;
	}
	tokens.push_back(string(token.begin(), token.end()));
}

void Parser::parsingInputFile(string inputFileName, Graph &graph){
	fstream inputStream(inputFileName, ios::in);
	if (!inputStream){
		cout << "Open file" << inputFileName << "fail." << endl;
		exit(1);
	}
	string lineBuffer;
	vector<vector<string>> adjacencyMatrix;
	vector<string> *tempTokens = nullptr;
	while (!inputStream.eof()){
		getline(inputStream, lineBuffer);
		if (!lineBuffer.empty()){
			tempTokens = new vector<string>();
			splitString(lineBuffer, *tempTokens);
			adjacencyMatrix.push_back(*tempTokens);
		}
	}
	Node *tempNode = nullptr;
	for (int i = 0; i < adjacencyMatrix.size(); i++){
		tempNode = new Node();
		graph.putNode(tempNode);
	}
	Edge *tempEdge = nullptr;
	int currentMaxEdgeLength = 0;
	for (int i = 0; i < adjacencyMatrix.size(); i++){
		for (int j = 0; j < adjacencyMatrix.at(i).size(); j++){
			if (adjacencyMatrix.at(i).at(j) != "0"){
				tempEdge = new Edge(i + 1, j + 1, graph.getNode(j), stoi(adjacencyMatrix.at(i).at(j)));
				graph.putEdgeToNode(i, tempEdge);
				if (stoi(adjacencyMatrix.at(i).at(j)) > currentMaxEdgeLength)
					currentMaxEdgeLength = stoi(adjacencyMatrix.at(i).at(j));
			}
		}
	}
	graph.setMaxEdgeLength(currentMaxEdgeLength);
	inputStream.close();
}