#ifndef __Edge_H__
#define __Edge_H__
#include "Node.h"
using namespace std;
class Node;
class Edge{
private:
	Node *nextNode;
	int prevNodeIndex;
	int nextNodeIndex;
	int distance;
public:
	Edge();
	//Edge(Node *nextNode);
	Edge(int prevNodeIndex, int nextNodeIndex, int distance);
	Edge(int prevNodeIndex, int nextNodeIndex, Node *nextNode, int distance);
	void setNextNode(Node *nextNode);
	Node *getNextNode();
	void setNextNodeIndex(int nextNodeIndex);
	int getNextNodeIndex();
	void setPrevNodeIndex(int prevNodeIndex);
	int getPrevNodeIndex();
	bool isNextNodeMarked();
	void setDistance(int distance);
	int getDistance();
	~Edge();
};
#endif