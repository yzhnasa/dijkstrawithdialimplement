#include "Main.h"

int main(int argc, char *argv[]){
	if (argc != 2){
		cout << "Please input inputfile and outputfile name." << endl;
		exit(1);
	}
	Graph *graph = new Graph();
	Parser::parsingInputFile(argv[1], *graph);
	graph->DijkstraShortestPath();
	graph->printNodeK();
	delete graph;
	return 0;
}