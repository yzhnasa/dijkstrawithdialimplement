#include "Edge.h"

Edge::Edge(){
	this->nextNode = nullptr;
}

Edge::Edge(int prevNodeIndex, int nextNodeIndex, int distance){
	this->prevNodeIndex = prevNodeIndex;
	this->nextNodeIndex = nextNodeIndex;
	this->nextNode = nullptr;
	this->distance = distance;
}

Edge::Edge(int prevNodeIndex, int nextNodeIndex, Node *nextNode, int distance){
	this->prevNodeIndex = prevNodeIndex;
	this->nextNodeIndex = nextNodeIndex;
	this->nextNode = nextNode;
	this->distance = distance;
}

void Edge::setNextNode(Node *nextNode){
	this->nextNode = nextNode;
}

Node *Edge::getNextNode(){
	return nextNode;
}

void Edge::setNextNodeIndex(int nextNodeIndex){
	this->nextNodeIndex = nextNodeIndex;
}

int Edge::getNextNodeIndex(){
	return nextNodeIndex;
}

void Edge::setPrevNodeIndex(int prevNodeIndex){
	this->prevNodeIndex = prevNodeIndex;
}

int Edge::getPrevNodeIndex(){
	return prevNodeIndex;
}

bool Edge::isNextNodeMarked(){
	if (nextNode->isMarked())
		return true;
	return false;
}

void Edge::setDistance(int distance){
	this->distance = distance;
}

int Edge::getDistance(){
	return distance;
}

Edge::~Edge(){

}